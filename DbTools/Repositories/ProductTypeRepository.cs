﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DbTools.DbContext;
using DbTools.Entities;

namespace DbTools.Repositories
{
    public class ProductTypeRepository : IRepository<ProductType, int>
    {
        private readonly DbConfigurator _dbConfigurator;

        public ProductTypeRepository(DbConfigurator dbConfigurator)
        {
            _dbConfigurator = dbConfigurator;
        }

        public ProductType GetById(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            string query = @"select top(1) * from [product_type] where [id] = @Id";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return PopulateFromReader(reader);
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool IsExist(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentNullException(nameof(id));
            }

            string query = @"select * from [product_type] where [id] = @Id";

            using (SqlConnection connction = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connction))
                    {
                        command.Parameters.AddWithValue("Name", id);

                        return command.ExecuteNonQuery() >= 1;
                    }
                }
                finally
                {
                    connction.Close();
                }
            }
        }

        public bool Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            string query = "delete from [product_type] where [id] = @Id";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Id", id);

                        return command.ExecuteNonQuery() >= 1;
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int Create(ProductType entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            string query = @"insert into [product_type] ([name]) values (@name)";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Name", entity.Name);

                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool Update(ProductType entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (entity.Id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(entity.Id));
            }

            string query = @"update [product_type] set [name] = @name where [id] = @Id";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Name", entity.Name);
                        command.Parameters.AddWithValue("Id", entity.Id);

                        return command.ExecuteNonQuery() >= 1;
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public IList<ProductType> GetAll()
        {
            string query = @"select * from [product_type] order by [id]";

            var dataList = new List<ProductType>();

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                dataList.Add(PopulateFromReader(reader));
                            }
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }

            return dataList;
        }

        private static ProductType PopulateFromReader(SqlDataReader reader)
        {
            return new ProductType
            {
                Name = reader["name"].ToString(),
                Id = (int) reader["id"]
            };
        }
    }
}
