﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DbTools.DbContext;
using DbTools.Entities;

namespace DbTools.Repositories
{
    public class ProductRepository : IRepository<Product, int>
    {
        private readonly DbConfigurator _dbConfigurator;

        public ProductRepository(DbConfigurator dbConfigurator)
        {
            this._dbConfigurator = dbConfigurator ?? throw new ArgumentNullException(nameof(dbConfigurator));
        }

        public Product GetById(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            string query = @"select [product].*, [product_type].[name] as [product_type_name] from [product] 
                            inner join [product_type] on [product_type].[id] = [product].[product_type_id]
                             where [product].[id] = @Id";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Id", id);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            return PopulateFromReader(reader);
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool IsExist(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            string query = "select * from [product] where [id] = @Id";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Id", id);

                        return command.ExecuteNonQuery() >= 1;
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            string query = "delete from [product] where [id] = @Id";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Id", id);

                        return command.ExecuteNonQuery() >= 1;
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int Create(Product entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            string query = @"insert into [product] ([name], [price], [rest], [product_type_id]) values (@Name, @Price, @Rest, @ProductTypeId)";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Name", entity.Name);
                        command.Parameters.AddWithValue("Price", entity.Price);
                        command.Parameters.AddWithValue("Rest", entity.Rest);
                        command.Parameters.AddWithValue("ProductTypeId", entity.ProductType.Id);

                        return Convert.ToInt32(command.ExecuteScalar());
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool Update(Product entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (entity.Id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(entity.Id));
            }

            if (entity.ProductType == null)
            {
                throw new ArgumentNullException(nameof(entity.ProductType));
            }

            string query = @"update [product] set 
                            [name] = @Name, 
                            [price] = @Price, 
                            [rest] = @Rest, 
                            [product_type_id] = @ProductTypeId
                            where [id] = @Id";

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("Id", entity.Id);
                        command.Parameters.AddWithValue("Name", entity.Name);
                        command.Parameters.AddWithValue("Price", entity.Price);
                        command.Parameters.AddWithValue("Rest", entity.Rest);
                        command.Parameters.AddWithValue("ProductTypeId", entity.ProductType.Id);

                        return command.ExecuteNonQuery() >= 1;
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public IList<Product> GetAll()
        {
            string query = @"select [product].*, [product_type].[name] [product_type_name] from [product] 
                            inner join [product_type] on [product_type].[id] = [product].[product_type_id]";

            var dataList = new List<Product>();

            using (SqlConnection connection = this._dbConfigurator.GetSqlConnection())
            {
                try
                {
                    using (var command = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                dataList.Add(PopulateFromReader(reader));
                            }
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
            }

            return dataList;
        }

        private Product PopulateFromReader(SqlDataReader reader)
        {
            return new Product
            {
                Id = (int)reader["id"],
                Name = reader["name"].ToString(),
                Price = Convert.ToDecimal(reader["price"]),
                ProductType = new ProductType
                {
                    Id = (int)reader["product_type_id"],
                    Name = reader["product_type_name"].ToString()
                },
                Rest = (int)reader["rest"]
            };
        }
    }
}
