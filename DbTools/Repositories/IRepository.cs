﻿using System.Collections.Generic;

namespace DbTools.Repositories
{
    interface IRepository<T, U>
    {
        T GetById(U id);
        bool IsExist(U id);
        bool Delete(U id);
        U Create(T entity);
        bool Update(T entity);
        IList<T> GetAll();
    }
}
