﻿using System;
using DbTools.Repositories;

namespace DbTools.DbContext
{
    public class DbContext
    {
        private DbConfigurator _dbConfigurator;

        public ProductRepository Product => new ProductRepository(this._dbConfigurator);
        public ProductTypeRepository ProductType => new ProductTypeRepository(this._dbConfigurator);

        public DbContext(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

           this._dbConfigurator = new DbConfigurator(connectionString);
        }
    }
}
