﻿using System;
using System.Data.SqlClient;

namespace DbTools.DbContext
{
    public class DbConfigurator
    {
        private readonly string _connectionString;

        public DbConfigurator(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            this._connectionString = connectionString;
        }

        public SqlConnection GetSqlConnection(bool isOpen = true)
        {
            var connection = new SqlConnection(this._connectionString);

            if (isOpen)
            {
                connection.Open();
            }

            return connection;
        }
    }
}
