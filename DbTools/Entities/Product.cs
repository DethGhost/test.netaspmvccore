﻿namespace DbTools.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Rest { get; set; }
        public ProductType ProductType { get; set; }
    }
}
