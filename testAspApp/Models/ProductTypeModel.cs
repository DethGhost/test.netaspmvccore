﻿namespace testAspApp.Models
{
    public class ProductTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
