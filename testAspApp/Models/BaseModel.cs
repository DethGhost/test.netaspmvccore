﻿using System.Collections.Generic;

namespace testAspApp.Models
{
    public class BaseModel
    {
        public IList<ProductTypeModel> TypeModels { get; set; }
        public IList<ProductModel> ProductModels { get; set; }
        public SubmitModel SubmitModel { get; set; }
    }
}
