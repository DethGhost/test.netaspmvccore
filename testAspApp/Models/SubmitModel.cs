﻿using System.Collections.Generic;

namespace testAspApp.Models
{
    public class SubmitModel
    {
        public ProductModel Product { get; set; }
        public IList<ProductTypeModel> Types { get; set; }
        public ProductTypeModel ProductType { get; set; }
    }
}
