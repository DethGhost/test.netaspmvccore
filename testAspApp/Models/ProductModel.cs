﻿namespace testAspApp.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Rest { get; set; }
        public ProductTypeModel ProductType { get; set; }
    }
}
