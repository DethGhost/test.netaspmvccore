﻿using System;
using DbTools.Entities;
using testAspApp.Models;

namespace testAspApp.Classes
{
    public class ClassMapper
    {
        public static ProductTypeModel GetProductTypeModel(ProductType entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return new ProductTypeModel
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public static ProductType GetProductTypeEntity(ProductTypeModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            return new ProductType
            {
                Id = model.Id,
                Name = model.Name
            };
        }

        public static ProductModel GetProductModel(Product entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return new ProductModel
            {
                Id = entity.Id,
                Name = entity.Name,
                ProductType = GetProductTypeModel(entity.ProductType),
                Rest = entity.Rest,
                Price = entity.Price
            };
        }

        public static Product GetProductEntity(ProductModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            return new Product
            {
                Id = model.Id,
                Name = model.Name,
                ProductType = GetProductTypeEntity(model.ProductType),
                Rest = model.Rest,
                Price = model.Price
            };
        }
    }
}
