﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(document).ready(() => {
    $(document).on('submit', '#submit-product-type', (event) => {
            event.stopPropagation();
            event.preventDefault();

            const form = $('#submit-product-type');
            const url = form.attr('action');
            const data = form.serialize();

        sendAjaxAndReplaceTable(url, data, '#product-type-table', form.attr('method'));
        });

    $(document).on('submit', '#submit-product', (event) => {
        event.stopPropagation();
        event.preventDefault();

        const form = $('#submit-product');
        const url = form.attr('action');

        sendAjaxAndReplaceTable(url, form.serialize(), '#product-table', form.attr('method'));
    });

    $(document).on('click', '.delete', (event) => {
        const element = $(event.currentTarget);
        const url = element.data('url');
        const id = element.data('id');
        const table = element.closest('table');

        deleteItem(url, id, table);
    });

    $(document).on('click', '.edit-product', (event) => {
        const element = $(event.currentTarget);
        const url = element.data('url');
        const id = element.data('id');
        const table = element.closest('table');

        placeEditProduct(url, id, table);
    });
});

function placeEditProduct(url, id, table) {
    const form = $('#submit-product');
    form.attr('action', url);
    form.attr('method', 'PUT');

    const tableRow = table.find(`#item-${id}`);
    const productId = tableRow.find('#product-cell-id').text();
    const productName = tableRow.find('#product-cell-name').text();
    const productTypeId = tableRow.find('#product-cell-type').data('id');
    const productRest = tableRow.find('#product-cell-rest').text();
    const productPrice = tableRow.find('#product-cell-price').text();
    form.find('#Product_Id').val(productId);
    form.find('#Product_Name').val(productName);
    form.find('#Product_Rest').val(productRest);
    form.find('#Product_Price').val(productPrice);
    form.find('#Product_ProductType_Id').val(productTypeId).change();
}

function sendAjaxAndReplaceTable(url, data, idToReplace, method) {
    $.ajax({
        url: url,
        method: method,
        data: data,
        success: (data) => {
            if (data) {
                const table = $(idToReplace);

                table.replaceWith(data);
            }
        }
    });
}

function deleteItem(url, id, table) {
    $.ajax({
        url: `${url}?id=${id}`,
        method: 'DELETE',
        success: (result) => {
            if (result.success) {
                table.find(`#item-${id}`).remove();
            }
        }
    });
}
