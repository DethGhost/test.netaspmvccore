﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DbTools.DbContext;
using Microsoft.AspNetCore.Mvc;
using testAspApp.Classes;
using testAspApp.Models;

namespace testAspApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly DbContext _context;

        public HomeController(DbContext context)
        {
            this._context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IActionResult Index()
        {
            IList<ProductTypeModel> types = this._context.ProductType.GetAll().Select(ClassMapper.GetProductTypeModel)
                .ToList();

            var baseModel = new BaseModel
            {
                TypeModels = types,
                ProductModels = this._context.Product.GetAll().Select(ClassMapper.GetProductModel).ToList(),
                SubmitModel = new SubmitModel
                {
                    Types = types
                }
            };

            return View(baseModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult AddProductType(SubmitModel model)
        {
            if (model.ProductType == null)
            {
                throw new ArgumentNullException(nameof(model.ProductType));
            }

            this._context.ProductType.Create(ClassMapper.GetProductTypeEntity(model.ProductType));

            IList<ProductTypeModel> models = this._context.ProductType.GetAll().Select(ClassMapper.GetProductTypeModel).ToList();
            return PartialView("_ProductTypesTable", models);
        }

        [HttpPost]
        public IActionResult AddProduct(SubmitModel model)
        {
            if (model.Product == null)
            {
                throw new ArgumentNullException(nameof(model.Product));
            }

            this._context.Product.Create(ClassMapper.GetProductEntity(model.Product));
            List<ProductModel> data = this._context.Product.GetAll().Select(ClassMapper.GetProductModel).ToList();

            return PartialView("_ProductsTable", data);
        }

        [HttpDelete]
        public IActionResult DeleteProductType(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            return Json(new {Success = this._context.ProductType.Delete(id)});
        }

        [HttpDelete]
        public IActionResult DeleteProduct(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            return Json(new {Success = this._context.Product.Delete(id)});
        }

        [HttpPut]
        public IActionResult EditProduct(SubmitModel model)
        {
            if (model.Product == null)
            {
                throw new ArgumentNullException(nameof(model.Product));
            }

            if (this._context.Product.Update(ClassMapper.GetProductEntity(model.Product)))
            {
                IList<ProductModel> data = this._context.Product.GetAll().Select(ClassMapper.GetProductModel).ToList();
                return PartialView("_ProductsTable", data);
            }

            return Json(false);
        }
    }
}
